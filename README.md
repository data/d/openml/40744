# OpenML dataset: data-v3.en-es-IRIs.clean2.anno_without_14top.uniform.arff

https://www.openml.org/d/40744

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

EN-ES-IRI annotations. Uniform to be compared to EN-ES-lit

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40744) of an [OpenML dataset](https://www.openml.org/d/40744). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40744/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40744/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40744/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

